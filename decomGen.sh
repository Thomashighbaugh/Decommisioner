#! /bin/sh

# outputs a rapid install script in record time!
for APT in `find /etc/apt/ -name \*.list`; do
    grep -Po "(?<=^deb\s).*?(?=#|$)" $APT | while read ENTRY ; do
        HOST=`echo $ENTRY | cut -d/ -f3`
        USER=`echo $ENTRY | cut -d/ -f4`
        PPA=`echo $ENTRY | cut -d/ -f5`
        #echo sudo apt-add-repository ppa:$USER/$PPA

	# conditional one determines if it is a ppa 
        if [ "ppa.launchpad.net" = "$HOST" ]; then
            echo sudo apt-add-repository ppa:$USER/$PPA -y
        # if not a PPA, the repo is added with an adjusted format
	else
            echo sudo apt-add-repository \'${ENTRY}\' -y
        fi
    done
done
