#!/bin/bash
echo "Author: Thomas Leon Highbaugh"
echo "Title: The Decommissioner"
echo "Purpose: Create Install Scripts from Current System for a New One"
#calls a script that renders a bash install script based on repos installed
sudo sh decomGen.sh > install-repo.sh &

# creates a text file listing those repos
mkdir output/
sudo sh decomGen.sh > output/install-repos.txt &

#Creates a text file of all the currently installed packages
dpkg -l | grep ^ii | awk '{print $2}'> output/install-pkgs.txt


# creates an array with the names of those packages stripped of extra information (in this case the word
# installed listed after each package
list=$(dpkg --list \* | awk '/ii/{print $2}')

#creates a file for package install script and begins with the shebang
echo "#!/bin/bash" > install-pack-attack.sh

# loops through the array appending the name to an apt auto command then adding that command to the file
while IFS= read -r pkg
do
echo "sudo apt-get install -y " $pkg >> install-pack-attack.sh
done <<< "$list"

# creates a file that will install the repos and then packages
echo "#!/bin/bash" > install.sh
echo "sudo chmod +x *" >> install.sh
echo "sudo bash install-repo.sh &" >> install.sh
echo "wait" >> install.sh
echo "sudo bash install-pack-attack.sh &" >> install.sh

#creates a directory, moves manifests and install scripts to this new directory then zips them up with a one line instruction file
mkdir install-scripts
mv install.sh install-scripts/
mv install-repo.sh install-scripts/
mv install-pack-attack.sh install-scripts/
cp -r output/ install-scripts/
echo "To install, just use chmod +x * then bash install.sh " > Instructions.md
mv Instructions.md install-scripts/
sudo tar -zcvf install.tar.gz install-scripts/

# clean up
sudo rm -fr install-scripts/
sudo rm -fr output/1111111111
mv install-scripts.tar.gz ~/Desktop/

