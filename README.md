# Decommisioner
Script to automatically generate complete package and repository recovery scripts.


# Introduction 
The executables in this repo are two short but potent scripts for use in the reprovisioning of my workstation as I work out the kinks in the OSes that I regularly switch between in preparation for my own OS coming soon. I find it is helpful to be able to restore the complete system from a point that is different on my various machines and the autoinstaller approach was often limited in effectiveness and meant several additional installs afterwards. Using these scripts, I can restore my system to its optimal conditions, including adding PPAs, with the same automatic convenience but without needing more than the snap autoinstall. (Look for all of these to be packaged into a bundle soon)  

By running the scripts you will create a compressed directory that will contain a script to reprovision your PPAs/ Repos and one that will reprovision your packages themselves. These are separated in case some package adjustments were desired still. 

---

## Use Cases
This script is not a one size fits all script, it backs up **no data** nor does do the scripts do the other things I would prefer (but make no sense offering in a general setting) like copying my fonts, icons and themes (which I have hundreds of). 

-1 When copying a specific dev environment when it has reached some perfected state of configuration

-2 Before attempting a new configuration on your homelab 

-3 Provisioning Identical Systems Across a Network Based on One or Several machine's configuration 

-4 Audit what repositories and packages are even on your system by getting lists generated of both as part of the programs functionality

etc etc etc 

---

## Requirements and Installation 

### Requirements
>Operating System: Debian, Ubuntu or Derivative thereof such as popOS, Linux Mint, Voyager, etc
  >The script uses the DPKG and APT package managers to configure the install script as well as within the install script. While
 pacman & alpine apk versions forthcoming (as well as an RPM & possibly a homebrew version in time), I have started with what I use most often.
 Dependencies: Nothing except the "Linux powertools" grep, awk, sed & friends
 
 ### Installation
 Download the package from here or on command line via 
 
 ```
 git clone https://github.com/Thomashighbaugh/Decommisioner.git
 
 ```
 
 then cd into the directory and you are ready to rock and roll. 
 
 ## Usage 
 >To use the script, make it executable with
 
 ```
 sudo chmod +x make-script.sh 
 ```
 >then use run it with
 
 ```
 sudo bash make-script.sh
 ```
 
 **Note** Run it with the less elegant bash command if you want it to be fully functional sorry ./ fans (stupid to pretend you save much time not typing two characters anyway)!
 
 Within 30 seconds (hardware dependent) you'll have an install script you can bring to any other apt-based system giving you your current repos and packages quickly and easily
 
 ### Running the install script
 On the new machine extract the zip file andrun the script lasbeled install.sh
 
 ```
 sudo bash install.sh 
 ```
 This will handle the repos and packages (in that order)
 
 ## Limitations
 - The script is not refactored elegance nor is it intended to be. It is to do its job and nothing more. As such, any refactoring you want to do, please fork it and fix it (I'll be happy to sing your praises for it)
 
 - Not all packages came from PPAs or Repos, some are from personal .deb archives and others from Snap or Flatpak. A script to install all of those and the script generated here is forthcoming
 
- Not a true shortcoming but no its not an Arch Linux based program, one is coming but if one couldn't tell after seeing the PKGMNGER being APT and DPKG based didn't clue you into that, you really don't know Linux and should hop off the bandwagon and try new distros to really get a feeling for the OS vs. Devs vs Flavor of the Environment before more Arch evangelism. Awesome system for those that like rolling release just too prone to being broken and inconsistent for me to keep on most of my systems.
